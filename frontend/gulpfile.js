const {src, dest, watch, parallel } = require("gulp");
const sassGulp = require("gulp-sass");
const postcss = require("gulp-postcss");
const purgecss = require("gulp-purgecss");
const del = require("del");
const nodemon = require("gulp-nodemon");
const browserSync = require("browser-sync").create();
sassGulp.compiler = require("node-sass");

// BULMA SASS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
bulmaTask = () => {
	return src("assets/scss/style.scss", {sourcemaps: true})
		.pipe(sassGulp().on("error", sassGulp.logError))
		.pipe(postcss([
			require("autoprefixer"),
		]))
		.pipe(purgecss({
			content: [
				"confirmation.html",
				"index.html",
				"panier.html",
				"assets/js/main.js",
				"assets/js/confirmation.js",
				"assets/js/index.js",
				"assets/js/panier.js"
			],
		}))
		.pipe(dest("assets/css", {sourcemaps: "."}));
};

// Fontawesome xxxxxxxxxxxxxxxxxxxxxxxxxx
fontTask = () => {
	return src(["./node_modules/@fortawesome/fontawesome-free/webfonts/fa-brands-400.woff2",
		"./node_modules/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2",
		"./node_modules/@fortawesome/fontawesome-free/webfonts/fa-regular-400.woff2"])
		.pipe(dest("assets/webfonts"));
}

// CLEAN xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
cleanTask = () => {
	return del("assets/css/style.*");
};

// BROWSERSFIELD xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
browserSyncServe = (cb) => {
	browserSync.init({
		server:{ baseDir: "./"}
	});
	cb();
};

browserSyncReload = (cb) => {
	browserSync.reload();
	cb();
};

// NODEMOM xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
nodemonTask = () => {
	nodemon({
		exec: "node --inspect",
		script: "asset/js/index.js",
		ext: "html js",
		tasks: ["lint"],
		env: {"NODE_ENV": "development"}
	});
};

// WATCH xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
watchBulma = () =>{
	watch("*.html", browserSyncReload);
	watch ("assets/scss/*.scss", bulmaTask, browserSyncReload);
	watch ("assets/js/*.js", browserSyncReload);
};

// EXPORTS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
exports.clean = cleanTask;
exports.bulma = bulmaTask;
exports.watchBulma = parallel(browserSyncServe, watchBulma);
exports.font = fontTask;
exports.nodemon = nodemonTask;
