// RECUPERER ID DE LA SELECTION ::::::::::::::::::::::::::::::::::::::::::::
const urlCamera =  document.location.href;
const cameraId = urlCamera.substring(urlCamera.lastIndexOf("?id=") + 4);

mainCamera();
// FONCTION PRINCIPALE :::::::::::::::::::::::::::::::::::::::::::::::::::::
async function mainCamera(){
	const camera = await getCamera();
	await buildCamera(camera);
	let btn = document.getElementById("button");
	btn.addEventListener("click", () => {
		let cameraBuy = {
			_id: camera._id,
			name: camera.name,
			imageUrl: camera.imageUrl,
			price: camera.price,
			lenses: camera.lenses,
			quantity: 1,
			totalPrice: camera.price
		};
		addCameraToLocal(cameraBuy);
		console.log(cameraBuy);
	});
}

// SELECTIONNER L'OBJET CAMERA ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function getCamera(){
	return fetch("http://localhost:3000/api/cameras/" + cameraId)
		.then(response => {
			if(response.ok){
				return response.json();
			}
			else{
				console.log("Mauvaise réponse du réseau");
			}
		})
		.then(camera => {
			console.log(camera);
			return camera;
		})
		.catch(error =>{
			console.log(error);
		});
}

// AFFICHER LA PAGE DU PRODUIT SELECTIONNEE ::::::::::::::::::::::::::::::::::::::::::::::::::
function buildCamera(camera){
	// Afficher le titre de la caméra choisie 
	let titleProd = document.querySelector("h2.title.is-2");
	titleProd.textContent = camera.name;

	// Afficher l'image de la caméra choisie 
	let imageProd = document.querySelector("div.column.is-8");
	imageProd.innerHTML = `<img src="${camera.imageUrl}"/>`;

	// Afficher les textes de description de la Caméra choisie
	let descriptionProd_1 = document.querySelector("div.content > p");
	descriptionProd_1.textContent = camera.description;
	let descriptionProd_2 = document.querySelector("div.content > h3+p");
	descriptionProd_2.textContent = camera.description;

	// Afficher le prix de la Caméra en Euro
	let priceProd = document.querySelector("button.button.is-outlined");
	priceProd.textContent = priceEuro(camera.price);

	// Afficher et selectionner les lentilles
	let lensesCamera = document.getElementById("objLens");
	for(let i=0; i < camera.lenses.length; i++){
		lensesCamera.insertAdjacentHTML("beforeend", `<option value="${camera.lenses[i]}"> ${camera.lenses[i]}</option>`);
		console.log(lensesCamera);
		// Selectionner la lentille à envoyer
		lensesCamera.addEventListener("change", function() {
			camera.lenses = lensesCamera.value;
			console.log(camera.lenses);
		});
	}
}

// CALCUL DU PRIX TOTAL :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
let cameraBuys = [];
function addCameraToLocal(cameraBuy) {
	let allCameras = JSON.parse(localStorage.getItem("prdInCart"));
	if(allCameras === null){
		cameraBuys.push(cameraBuy); 
		localStorage.setItem("prdInCart", JSON.stringify(cameraBuys));
	}else{
		allCameras.forEach(camera => {
			if(cameraBuy.name == camera.name){
				cameraBuy.totalPrice += camera.totalPrice;
				cameraBuys.push(camera);
			}else{
				cameraBuys.push(camera);
			}
		});
		cameraBuys.push(cameraBuy);
	}
	localStorage.setItem("prdInCart", JSON.stringify(cameraBuys));
	window.location.reload();
}
