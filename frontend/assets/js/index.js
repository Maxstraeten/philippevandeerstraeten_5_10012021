mainCameras();
// FONCTION PRINCIPALE ::::::::::::::::::::::::::::::::::::::::::::
async function mainCameras(){
	const allCameras = await getAllcameras();
	await displayAllCameras(allCameras);
}

// RECUPERER LES DONNEES DE L'API CAMERAS :::::::::::::::::::::::::::
function getAllcameras(){
	return fetch("http://localhost:3000/api/cameras/")
		.then(response => {
			if(response.ok){
				return response.json();
			}
			else{
				console.log("Mauvaise réponse du réseau");
			}
		})
		.then(allCameras => {
			console.log(allCameras);
			return allCameras;
		})
		.catch(error => console.log("Request Failed", error));
}

// AFFICHER LES CAMERAS SUR LA PAGE ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function displayAllCameras(allCameras){
	// Inverser les données de l'API
	allCameras.reverse();
	// Afficher les objets de l'API sur la page
	allCameras.find(camera =>{
		document.getElementById("colcard").insertAdjacentHTML("afterbegin",
			`<div class="column is-one-third-widescreen is-half-desktop">
			<div class="card">
				<div class="card-image">
					<figure class="image is-4by3">
					<img src="${camera.imageUrl}"/>
					</figure>
				</div>
				<div class="card-content">
					<h3 class="title is-4">${camera.name}</h3>
					<h4 class="subtitle is-6">occasion</h4>
				<div class="buttons has-addons are-medium is-right">
				<button class="button is-primary is-outlined">${priceEuro(camera.price)}</button> 
				<a href="produit.html?id=${camera._id}" class="button is-primary">
				<span class="icon"><i class="fas fa-shopping-cart"></i></span>
				<span class="is-hidden-mobile">Achetez</span></a>
				</div>
			</div>
		</div>`);
	});
}
