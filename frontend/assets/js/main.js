// CREATION DE LA NAVBAR BURGER ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
document.addEventListener("DOMContentLoaded", () => {
	// Get all "navbar-burger" elements
	const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll(".navbar-burger"), 0);
	// Check if there are any navbar burgers
	if ($navbarBurgers.length > 0) {
		// Add a click event on each of them
		$navbarBurgers.forEach( el => {
			el.addEventListener("click", () => {
				// Get the target from the "data-target" attribute
				const target = el.dataset.target;
				const $target = document.getElementById(target);
				// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
				el.classList.toggle("is-active");
				$target.classList.toggle("is-active");
			});
		});
	}
});

// AFFICHER LE NOMBRE D'ARTICLES SELECTIONNES DANS LE BARRE DE NAVIGATION ::::::::::::::::::::::
function cameraNumberDisplay(){
	let totalQuantity = 0;
	let allCameras = JSON.parse(localStorage.getItem("prdInCart"));
	allCameras.forEach(camera => totalQuantity += camera.quantity);
	console.log(totalQuantity);
	document.querySelector("button > .icon.has-text-weight-bold").textContent = totalQuantity;
}
cameraNumberDisplay();

// TRANFORMER UN CHIFFRE EN UN PRIX EURO :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function priceEuro(price){
	return Number.parseFloat(price/100).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$& ") + " €";
}
