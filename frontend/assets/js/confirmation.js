// RECUPERER LES INFORMATIONS ET LES AFFICHER ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
let confirmOrder = JSON.parse(sessionStorage.getItem("order"));
console.log(confirmOrder);
document.querySelector("h4 > span.has-text-primary").textContent = confirmOrder.orderId;
console.log(confirmOrder.orderId);
document.querySelector("p > span.is-capitalized").textContent = confirmOrder.contact.firstName;
console.log(confirmOrder.contact.firstName);
document.querySelector("p > span.is-uppercase").textContent = confirmOrder.contact.lastName;
console.log(confirmOrder.contact.lastName);

// CALCULER LE PRIX TOTAL DU PANIER A AFFICHER DANS LA CONFIRMATION DE COMMANDE ::::::::::::::::::::::::::::::::
confirmOrder.products.totalPrice = 0;
confirmOrder.products.forEach(confirmOrderproduct => {
	confirmOrder.products.totalPrice += confirmOrderproduct.price;
	document.querySelector("p > span.has-text-weight-bold").textContent = priceEuro(confirmOrder.products.totalPrice);
});
console.log(confirmOrder.products.totalPrice);

sessionStorage.removeItem("order");
