dispCartItem();
cartPrice();
buttonDelete();


// AFFICHER LE CONTENU DU PANIER :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function dispCartItem(){
	let index = 0;
	// Récupération du panier dans le localStorage
	let allCameras = JSON.parse(localStorage.getItem("prdInCart"));
	// Affichage du panier et mise en forme du contenu dans la page html
	allCameras.forEach(camera => {
		document.getElementById("tbody").insertAdjacentHTML("beforebegin",
			`<tr class="has-text-centered">
                <td class="is-vcentered is-hidden">${index++}</td> 
                <td class="is-vcentered has-text-weight-bold">${camera.name}</td>
                <td class="image is-2by1 is-hidden-mobile"><img src="${camera.imageUrl}" alt="${camera.name}"></td>
                <td class="is-vcentered is-hidden-mobile">${camera.lenses}</td>
                <td class="is-vcentered is-hidden-mobile">${camera.quantity}</td>
                <td class="is-vcentered has-text-weight-bold">${priceEuro(camera.price)}</td>
                <td class="is-vcentered"><div class="button is-danger is-small"><span class="icon"><i class="fas fa-times">
                </i></span><span class="has-text-weight-bold is-uppercase">delete</span></div></td>
            </tr>`);
	});
}

// AFFICHER LE PRIX TOTAL DU PANIER :::::::::::::::::::::::::::::::
function cartPrice(){
	let priceTotal = 0;
	let allCameras = JSON.parse(localStorage.getItem("prdInCart"));
	if(allCameras != 0 ){
		allCameras.map(camera => priceTotal += camera.price);
		console.log(priceTotal);
		document.querySelector("h1.title").innerHTML = `Votre panier total est de <span class="has-text-primary">${priceEuro(priceTotal)}</span>`;
	}else{
		document.querySelector("h1.title").textContent = "Votre panier est vide";
	}
}

// SUPPRIMER ARTICLE PANIER ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function buttonDelete(){
	let cartTable = document.querySelector("table");
	let cartRemove = cartTable.querySelectorAll("div.button > .fa-times, div.button > span.is-uppercase");
	for(let i=0; i< cartRemove.length; i++ ){
		let cartDelete = cartRemove[i];
		console.log(cartDelete);
		let cameraBuys= [];
		cartDelete.addEventListener("click", e => {
			e.preventDefault();
			let allCameras = JSON.parse(localStorage.getItem("prdInCart"));
			allCameras.forEach((camera,index) =>{
				if(index != e.target.parentElement.parentElement.parentElement.children[0].textContent){
					cameraBuys.push(camera);
					console.log(camera);
				}
			});
			localStorage.setItem("prdInCart", JSON.stringify(cameraBuys));
			window.location.reload();
		});
	}
}

// CONFIRMATION DE LA COMMANDE :::::::::::::::::::::::::::::::::::
let buttonAction = document.getElementById("formulaire");
buttonAction.addEventListener("submit", confirmCommand);
function confirmCommand(e){
	e.preventDefault();
	let form = document.forms[0];
	let data = new FormData(form);
	let contact = {};
	// Récupération des informations du formulaire à l'aide de Formdata
	data.forEach((value, key) => {
		if(!Reflect.has(contact, key)){
			contact[key] = value;
			return;
		}
		if(!Array.isArray(contact[key])){
			contact[key] = [contact[key]];
		}
		contact[key].push(value);
	});
	validForm(contact);
	console.log(validForm(contact));
	console.log(contact);
	// Récupération des ID des caméras sélectionnées du panier
	let productId = JSON.parse(localStorage.getItem("prdInCart"));
	let products = productId.map(product => String(product._id));
	console.log(products);
	// Construction d'un Nouvel object : association de contact et caméras
	let objData = {
		contact,
		products
	};
	// 
	fetch("http://localhost:3000/api/cameras/order", {
		method: "POST",
		headers: {
			"Accept": "application/json", 
			"Content-Type": "application/json"
		},
		body: JSON.stringify(objData),
	}).then(response =>{
		if (response.ok) {
			return response.json();
		} else{
			console.log("Mauvaise réponse du réseau");
		}
	}).then(data =>{
		console.log(data);
		// validForm(contact) == false non validation du formulaire
		if(!validForm(contact)){
			let dangerForm = "<div class='box has-text-centered has-text-danger has-background-danger-light'>Formulaire non envoyé</div>";
			document.getElementById("dangerForm").innerHTML = dangerForm;
			e.preventDefault();
		}
		// validForm(contact) == true validation du formulaire
		else if(validForm(contact)){
			window.location = "./confirmation.html";
			sessionStorage.setItem("order", JSON.stringify(data));
		}
	}).catch(error => console.log("Request Failed", error));
}

// VALIDATION DES INPUT DU FORMULAIRE ::::::::::::::::::::::::::::::::::::
function validForm(){
	let firstName = document.querySelector("[name=firstName]");
	let lastName = document.querySelector("[name=lastName]");
	let address = document.querySelector("[name=address]");
	let city = document.querySelector("[name=city]");
	let email = document.querySelector("[name=email]");
	
	// Regex
	let emailRegex = /^[a-z0-9.-_]+[@]{1}[a-z0-9.-_]+[.]{1}[a-z]{2,10}?$/g;
	let globalRegex = /^[a-zA-ZéèêëîïÉÈËÎÏ][a-zéèêëàçîï]+([-'\s][a-zA-ZéèêëîïÉÈËÎÏ][a-zéèêëàçîï]+)?$/g;
	let globalRegex1 = /^[a-zA-ZéèêëîïÉÈËÎÏ][a-zéèêëàçîï]+([-'\s][a-zA-ZéèêëîïÉÈËÎÏ][a-zéèêëàçîï]+)?$/g;
	let globalRegex2 = /^[a-zA-ZéèêëîïÉÈËÎÏ][a-zéèêëàçîï]+([-'\s][a-zA-ZéèêëîïÉÈËÎÏ][a-zéèêëàçîï]+)?$/g;

	// Input FirstName 
	if(firstName.value.trim() === ""){
		let erreur = "Veuillez renseigner le champ prénom";
		document.querySelector("p#firstName-text.help").textContent = erreur;
		document.querySelector("p#firstName-text.help").style.color = "hsl(348, 100%, 61%)";
		firstName.classList.remove("is-success");
		firstName.classList.add("is-danger");
		return false;
	}
	if(!globalRegex.test(firstName.value)){
		let erreur = "Le prénom doit comporter des lettres et des tirets uniquement.";
		document.querySelector("p#firstName-text.help").textContent = erreur;
		document.querySelector("p#firstName-text.help").style.color = "hsl(348, 100%, 61%)";
		firstName.classList.remove("is-success");
		firstName.classList.add("is-danger");
		return false;
	}
	// Input LastName
	if(lastName.value.trim() === ""){
		let erreur = "Veuillez renseigner le champ nom";
		document.querySelector("p#lastName-text.help").textContent = erreur;
		document.querySelector("p#lastName-text.help").style.color = "hsl(348, 100%, 61%)";
		lastName.classList.remove("is-success");
		lastName.classList.add("is-danger");
		return false;
	}
	if(!globalRegex1.test(lastName.value)){
		let erreur = "Le nom doit comporter des lettres et des tirets uniquement.";
		document.querySelector("p#lastName-text.help").textContent = erreur;
		document.querySelector("p#lastName-text.help").style.color = "hsl(348, 100%, 61%)";
		lastName.classList.remove("is-success");
		lastName.classList.add("is-danger");
		return false;
	}
	// Input Address
	if(address.value.trim() === ""){
		let erreur = "Veuillez renseigner le champ adresse";
		document.querySelector("p#address-text.help").textContent = erreur;
		document.querySelector("p#address-text.help").style.color = "hsl(348, 100%, 61%)";
		address.classList.remove("is-success");
		address.classList.add("is-danger");
		return false;
	}
	// Input City
	if(city.value.trim() === ""){
		let erreur = "Veuillez renseigner le champ ville";
		document.querySelector("p#city-text.help").textContent = erreur;
		document.querySelector("p#city-text.help").style.color = "hsl(348, 100%, 61%)";
		city.classList.remove("is-success");
		city.classList.add("is-danger");
		return false;
	}
	if(!globalRegex2.test(city.value)){
		let erreur = "La ville doit comporter des lettres et des tirets uniquements.";
		document.querySelector("p#city-text.help").textContent = erreur;
		document.querySelector("p#city-text.help").style.color = "hsl(348, 100%, 61%)";
		city.classList.remove("is-success");
		city.classList.add("is-danger");
		return false;
	}
	// Input Email
	if(email.value.trim() === ""){
		let erreur = "Veuillez renseigner le champ email";
		document.querySelector("p#email-text.help").textContent = erreur;
		document.querySelector("p#email-text.help").style.color = "hsl(348, 100%, 61%)";
		email.classList.remove("is-success");
		email.classList.add("is-danger");
		return false;
	}
	if(!emailRegex.test(email.value)){
		let erreur = "Veuillez donner une adresse mail correcte.";
		document.querySelector("p#email-text.help").textContent  = erreur;
		document.querySelector("p#email-text.help").style.color = "hsl(348, 100%, 61%)";
		email.classList.remove("is-success");
		email.classList.add("is-danger");
		return false;
	}
	else{
		return true;
	}
}
