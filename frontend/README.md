## **Orinoco** ##

Ouvrir le terminal

Installer la dernière version de node js et npm

Cloner le projet : 

[Orinoco] (https://bitbucket.org/Maxstraeten/philippevandeerstraeten_5_10012021/src/master/)

Se placer dans le dossier:

- philippevandeerstraeten_5_10012021 

### **1. lancer le serveur en localhost 3000**

Dans le dossier : 

- philippevandeerstraeten_5_10012021/backend

Faire un `npm install` pour installer les packages node_modules

Lancer le serveur de l'API avec le code suivant

- `npm run start`

### **2. lancer le frontend du site Orinoco**

Dans le dossier :

- philippevandeerstraeten_5_10012021/frontend

Faire un `npm install` pour installer les packages node_modules

Lancer ensuite le site avec le script suivant :

- `gulp watchBulma`
